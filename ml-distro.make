core = 7.x
api = 2

; Drupal Core
projects[drupal][version] = 7.28
projects[drupal][patch][2033883] = http://drupal.org/files/orderedResults.patch

; ML Profile
projects[ml][type] = profile
projects[ml][download][type] = git
projects[ml][download][url] = git@bitbucket.org:minima-labs/ml-profile.git
projects[ml][download][branch] = 7.x-3.x
